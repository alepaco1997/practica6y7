package facci.aaron.parraga.practica6y7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadSensorAcelerometro extends AppCompatActivity {
    Button botonAcelerometro,botonProximidad,botonOrientacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_acelerometro);
        botonAcelerometro=(Button)findViewById(R.id.btnAcelerometro);
        botonProximidad=(Button)findViewById(R.id.btnProximidad);
        botonOrientacion=(Button)findViewById(R.id.btnOrientacion);

        botonOrientacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cambiar =new Intent(getApplicationContext(),Orientacion.class);
                startActivity(cambiar);
            }
        });

        botonProximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(ActividadSensorAcelerometro.this,Proximidad.class);
                startActivity(intent);
            }
        });
        botonAcelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cambiar =new Intent(ActividadSensorAcelerometro.this, Acelerometro.class);
                startActivity(cambiar);
            }
        });

    }

}
